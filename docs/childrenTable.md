###  ChildrenTable  通用子表组件
>  该组件主要用于被主表嵌套使用

### 可能使用的业务场景
> 1.采购合同,主界面主要填写采购人、采购人部门等，而同一个界面就可以使用子表填写 合同标的详情(采购清单、规格、物品参数等等)，一个界面完成一个完整的业务.


### 使用指南
>	[相关代码详情](../src/components/system-setting/system-menu/create_edit.vue)
```code

	// 1.引用组件
	import ChildrenTable from '_c/system-setting/common/children_table'
   // 导入
	export default {
  name: 'createEdit',
  components: {
    ChildrenTable
  }
  	//... 省略其他无关代码
  }

    // 界面引用
    <ChildrenTable :formInfo="formInfo"></ChildrenTable>

	// 2.定义子表的各项属性
	 formInfo: {
        //子表一条数据所拥有的字段定义
        formDataItem: {
          id:0,
          fr_auth_system_menu_id:0,
          fr_auth_button_cn_en_id:0,
          button_name:'',
          request_url:'/',
          request_method:"",
          remark:''
        },
        //子表字段定义： 目前每种字段数据类型可选项有5种——dialog（公共组件对话框）、text（文本）、select（列表）、upload（文件上传）
        tableCols:[
          {
            name:'按钮名称',//表单名称
            type : "dialog",//类型{弹出框}
            field:'button_name',//字段名
            componentPath : '_c/system-setting/common/button_cn_en',
            width:'6',//宽度,参考iview的col  row  ,一行值共计24
            modalWidth:'700px', // 弹出框宽度
            //字段与弹出框组件字段的映射
            map:{
              fr_auth_button_cn_en_id:'id',
              button_name:'cn_name',
              request_method:'allow_method'
            }
          },
          {
            name:'接口地址',
            type : "text",
            field:'request_url',
            width:'5',
          },
          {
            name:'请求方式',
            type : "select",
            field:'request_method',
            width:'4',
            option:[
              '*',
              'GET',
              'POST'
            ]
          },
          // 数字类型, 包括整数型、小数型
           {
             name:'数字',
             type : "number",
             field:'aa',
             width:'5',
           },
         {
           name:'文件上传',
           type : "upload",
           field:'file_path',
           width:'3',
         },
        ],

        //获取子表结果集数据，数组类型
        resultArr:[],

        //修改时,如果子表涉及到删除,则删除的ids在这里记录
        deleteString:'',

        //新增时给子表设置默认值
        defaultCreate : [
        {
          id:0,
          fr_auth_system_menu_id:0,
          fr_auth_button_cn_en_id:1,
          button_name:'新增',
          request_url:'/',
          request_method:"POST",
          remark:''
        },
        {
          id:0,
          fr_auth_system_menu_id:0,
          fr_auth_button_cn_en_id:2,
          button_name:'删除',
          request_url:'/',
          request_method:"POST",
          remark:''
        },
        {
          id:0,
          fr_auth_system_menu_id:0,
          fr_auth_button_cn_en_id:3,
          button_name:'修改',
          request_url:'/',
          request_method:"POST",
          remark:''
        },
        {
          id:0,
          fr_auth_system_menu_id:0,
          fr_auth_button_cn_en_id:4,
          button_name:'查询',
          request_url:'/',
          request_method:"GET",
          remark:''
        },
      ],
    },





```
